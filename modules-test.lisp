(defpackage :rjain.modules-test
  (:use :common-lisp :rjain.modules :xp-test)
  (:export #:*modules-test-suite*
           #:modules-test))

(in-package :rjain.modules-test)

(def-test-fixture modules-fixture ()
  ())

(defmethod setup ((fix modules-fixture))
  t)

(defmethod teardown ((fix modules-fixture))
  t)



(defparameter *modules-test-suite*
    (make-test-suite
     "Modules Test Suite"
     "Test suite for the modules system."))

(defun modules-test ()
  (report-result (run-test *modules-test-suite*) :verbose t))
