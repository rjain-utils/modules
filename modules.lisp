(defpackage :rjain.modules
  (:use :common-lisp)
  (:export #:module
           #:undefined-module
           #:defmodule
           #:remove-module
           #:with-module
           #:enable-module-reader-syntax
           #:disable-module-reader-syntax
           #:restore-module-reader-syntax
           #:locally-enable-module-reader-syntax
           #:locally-disable-module-reader-syntax))

(in-package :rjain.modules)

(defvar *modules* (make-hash-table :test #'eq :size 7 :rehash-size 1.2))

(deftype module () 'hash-table)

(define-condition undefined-module (cell-error)
  ())

(defun make-module (name &rest keys
                    &key size rehash-size #+(or sbcl cmu) weak-p)
  (apply #'make-hash-table :test #'eq keys))

(defun module (spec)
  (etypecase spec
    (symbol (restart-case
                (multiple-value-bind (module module-p)
                    (gethash spec *modules*)
                  (if module-p
                      module
                      (error 'undefined-module :name spec)))
              (define-module ()
                  :report "Define the module"
                  :interactive read-new-value)))
    (module spec)))

(defmacro defmodule (name supermodules &key documentation)
  (when supermodules
    (error "Module ~A defined with supermodules ~A, but this feature is not implemented."
           name supermodules))
  `(progn
     (ensure-module ',name)
     ,(when documentation
        `(setf (with-module ,name module-documentation) ,documentation))
     (module ',name)))

(defun ensure-module (name)
  (multiple-value-bind (module exists-p) (gethash name *modules*)
    (when (not exists-p)
      (setf (gethash name *modules*) (setf module (make-module name))))
    module))

(defun delete-module (name)
  (remhash name *modules*))

(defmacro with-module (module variable)
  `(nth-value 0 (gethash ',variable (module ',module))))

(defun set-module-value (value module variable)
  (setf (gethash variable (module module))
        value))

(define-setf-expander with-module (module variable)
  (let ((store (gensym)))
    (values () ()
            (list store)
            `(set-module-value ,store ',module ',variable)
            `(with-module ,module ,variable))))

(defmethod documentation ((name symbol) (doc-type (eql 'module)))
  (with-module name 'module-documentation))

(defmethod (setf documentation) (new-value (name symbol) (doc-type (eql 'module)))
  (setf (with-module name 'module-documentation) new-value))

(defun module-syntax-reader (s c n)
  (let ((spec (read s t nil t)))
    (if (consp spec)
        (destructuring-bind (module symbol) spec
          `(with-module ,module ,symbol))
        `(module ',spec))))

(defvar *module-reader-char* nil)

(defvar *original-reader-enter* nil)

(defvar *restore-module-reader-syntax* nil)

(defun %enable-module-reader-syntax (char readtable)
  (when (or *module-reader-char* *original-reader-enter*)
    (cerror "Clobber it" "Old defintion of syntax exists"))
  (setf *original-reader-enter*
        (get-dispatch-macro-character #\# char readtable)
        *module-reader-char*
        char)
  (set-dispatch-macro-character #\# char #'module-syntax-reader))

(defun %disable-module-reader-syntax ()
  (set-dispatch-macro-character
      #\# *module-reader-char* *original-reader-enter*)
  (setf *original-reader-enter* nil))

(defmacro enable-module-reader-syntax
    (&optional (char #\@) (readtable *readtable*))
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf *restore-module-reader-syntax* t)
     (%enable-sql-reader-syntax char readtable)))

(defmacro disable-module-reader-syntax ()
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf *restore-module-reader-syntax* nil)
     (%disable-module-reader-syntax)))

(defmacro locally-enable-module-syntax
    (&optional (char #\@) (readtable *readtable*))
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (%enable-module-reader-syntax)))

(defmacro locally-disable-module-reader-syntax ()
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (%disable-module-reader-syntax)))

(defmacro restore-module-reader-syntax ()
  '(eval-when (:compile-toplevel :load-toplevel :execute)
    (if *restore-module-reader-syntax*
        (%enable-module-reader-syntax)
        (%disable-module-reader-syntax))))

